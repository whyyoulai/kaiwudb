# KaiwuDB发版历史

## 1.0 

| 发版日期 | 2023.12. |
| ---- | ---------- |
| 发版说明 | [Release Notes](./1.0.md) |
| 代码 | [1.0](https://gitee.com/kaiwudb-opensource/kaiwudb) |

## 1.2

| 发版日期     | 2023.12.31 |
| -------- | ---------- |
| 发版说明 |            |
| 代码     |            |
