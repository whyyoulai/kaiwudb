# KaiwuDB Docs

English | [简体中文](./README.md)


Welcome to the official KaiwuDB documentation repository! 

Within this repository, you'll find a wealth of supporting documents for KaiwuDB, covering release notes, the KaiwuDB overview, administration guide, operation and maintenance guide, and the SQL reference manual.

## Contribute to the Documentation

We warmly welcome you to join us in crafting a more user-friendly KaiwuDB documentation! 

To get started, familiarize yourself with our [Contribution Process](https://gitee.com/kaiwudb-opensource/community/blob/master/Contribute_process.md), adhere to the [Style Guide](https://gitee.com/kaiwudb-opensource/docs/blob/master/style-guide.md), and submit pull requests (PR) according to the specified process. 

Once your first PR is merged, you officially become a valued contributor to the KaiwuDB documentation.

## Mapping between KaiwuDB Documentation and Folders

Delve into the `docs` repository to explore the connection between documents and folders, as detailed below:

| Folder            | Document                                     |
| :---------------- | :------------------------------------------- |
| about-kaiwudb     | KaiwuDB Overview                             |
| deployment        | Deployment Guide                             |
| db-administration | Administration Guide; Import and Export      |
| db-operation      | Operation and Maintenance Guide; Error Codes |
| sql-statements    | SQL Reference Manual                         |
| release-notes     | Release Notes                                |

## KaiwuDB Branches and Corresponding Documentation

Explore various branches within the `docs` repository, each linked to a specific KaiwuDB version:

| Branch | Documentation Version                  |
| :----- | :------------------------------------- |
| Master | Development branch, the default branch |
| V1.0   | Corresponding to KaiwuDB 1.0           |

## Contact us

Feel free to reach out to us via [docs@kaiwudb.org.cn](mailto:docs@kaiwudb.org.cn) for any inquiries or continued communication. We value your engagement and look forward to building a stronger KaiwuDB community together!