# KaiwuDB文档 

简体中文 | [English](./README.en.md)

欢迎来到KaiwuDB官方文档仓库！
此仓库存放的是KaiwuDB的支撑文档，包括发版说明、KaiwuDB简介、安装部署手册、管理指南、运维指南、SQL参考手册等内容。

## 贡献文档

我们非常欢迎您与我们一起建设更加易用、用户友好的KaiwuDB文档！
请阅读我们的[社区贡献](https://gitee.com/kaiwudb-opensource/community/blob/master/Contribute_process.md)，遵守[文档写作规范](./style-guide.md)，并按照流程规则提交。您提交的第一个 Pull Request (PR) 合并以后，即可成为KaiwuDB文档的贡献者。


## KaiwuDB文档夹与文档对应关系

`docs`文件夹包含了具体文档的内容。`docs`文件夹中各手册和文件夹对应关系如下：

| 文件夹名称        | 对应手册               |
| ----------------- | ---------------------- |
| about-kaiwudb     | KaiwuDB简介            |
| deployment        | 安装部署               |
| db-administration | 数据库管理；导入导出 |
| db-operation      | 数据库运维；错误码 |
| sql-statements    | SQL参考手册                |
| release-notes     | 发版说明               |


## KaiwuDB版本分支对应文档关系

文档仓库包含了如下分支：

| 文档仓库分支       | 文档版本              |
| ----------------- | ---------------------- |
| master            | 开发分支，为默认分支     |
| V1.0               | 对应KaiwuDB1.0版本       |

## 联系方式

邮件列表：docs@kaiwudb.org.cn